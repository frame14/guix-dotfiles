  (specifications->manifest
   (list
     "julia" ;; High-performance dynamic language for technical computing
     "julia-csv" ;; Fast and flexible delimited-file reader/writer
     "julia-dataframes" ;; In-memory tabular data
     "julia-http" ;; HTTP support for Julia
     "julia-gumbo" ;; Julia wrapper around Google's gumbo C library for parsing HTML
     "julia-json" ;; JSON parsing and printing library for Julia
     ))
