;;
;; Modules used
;;
(use-modules 
 (guix download)

 (guix build-system r)
 (guix build-system python)
 (guix build-system pyproject)

 (guix packages)
 (gnu packages cran)
 (gnu packages statistics)
 (gnu packages multiprecision)
 (gnu packages java)
 (gnu packages python-xyz)
 (gnu packages libffi)
 (gnu packages terminals)
 (gnu packages check)

 (guix licenses))



;;
;; Pakages definitions
;;
(define-public r-arrangements
  (package
   (name "r-arrangements")
   (version "1.1.9")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "arrangements" version))
            (sha256
             (base32
              "0nrlyxgn6667l2rb1q5rvps1anld9aq88qqr3ch2i6zchnqxrdg9"))))
   (properties `((upstream-name . "arrangements")))
   (build-system r-build-system)
   (inputs (list gmp))
   (propagated-inputs (list r-gmp r-r6))
   (home-page "https://github.com/randy3k/arrangements")
   (synopsis
    "Fast Generators and Iterators for Permutations, Combinations, Integer Partitions and Compositions")
   (description
    "Fast generators and iterators for permutations, combinations, integer partitions
and compositions.  The arrangements are in lexicographical order and generated
iteratively in a memory efficient manner.  It has been demonstrated that
arrangements outperforms most existing packages of similar kind.  Benchmarks
could be found at
<https://randy3k.github.io/arrangements/articles/benchmark.html>.")
   (license expat))
  )


(define-public r-iterpc
  (package
   (name "r-iterpc")
   (version "0.4.2")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "iterpc" version))
            (sha256
             (base32
              "06q7l8mz8ws4nn6gb0whnlqs8my2n8z2d2g8fvv3cxd28904dg9q"))))
   (properties `((upstream-name . "iterpc")))
   (build-system r-build-system)
   (propagated-inputs (list r-arrangements r-gmp r-iterators))
   (native-inputs (list r-knitr))
   (home-page "https://randy3k.github.io/iterpc")
   (synopsis "Efficient Iterator for Permutations and Combinations")
   (description
    "Iterator for generating permutations and combinations.  They can be either drawn
with or without replacement, or with distinct/ non-distinct items (multiset).
The generated sequences are in lexicographical order (dictionary order).  The
algorithms to generate permutations and combinations are memory efficient.
These iterative algorithms enable users to process all sequences without putting
all results in the memory at the same time.  The algorithms are written in C/C++
for faster performance.  Note: iterpc is no longer being maintained.  Users are
recommended to switch to arrangements'.")
   (license gpl2))
  )



(define-public r-refinr
  (package
   (name "r-refinr")
   (version "0.3.2")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "refinr" version))
            (sha256
             (base32
              "05jz3hgwghkbdi7r4977scifzcy5vrqmasjayq2mavada6dzj45l"))))
   (properties `((upstream-name . "refinr")))
   (build-system r-build-system)
   (propagated-inputs (list r-rcpp r-stringdist r-stringi))
   (native-inputs (list r-knitr))
   (home-page "https://github.com/ChrisMuir/refinr")
   (synopsis "Cluster and Merge Similar Values Within a Character Vector")
   (description
    "These functions take a character vector as input, identify and cluster similar
values, and then merge clusters together so their values become identical.  The
functions are an implementation of the key collision and ngram fingerprint
algorithms from the open source tool Open Refine <https://openrefine.org/>.
More info on key collision and ngram fingerprint can be found here
<https://docs.openrefine.org/next/technical-reference/clustering-in-depth/>.")
   (license gpl3))
  )

(define-public r-robustrao
  (package
   (name "r-robustrao")
   (version "1.0-5")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "robustrao" version))
            (sha256
             (base32
              "1r8v9qjx70s8swh09imq9kzdxg0qhnmk00zrg939mig9b12mw1p6"))))
   (properties `((upstream-name . "robustrao")))
   (build-system r-build-system)
   (propagated-inputs (list r-doparallel
                            r-foreach
                            r-gmp
                            r-igraph
                            r-iterpc
                            r-quadprog))
   (home-page "https://gitlab.com/mc.calatrava.moreno/robustrao.git")
   (synopsis "An Extended Rao-Stirling Diversity Index to Handle Missing Data")
   (description
    "This package provides a collection of functions to compute the Rao-Stirling
diversity index (Porter and Rafols, 2009) <DOI:10.1007/s11192-008-2197-2> and
its extension to acknowledge missing data (i.e.,        uncategorized references) by
calculating its interval of uncertainty using   mathematical optimization as
proposed in Calatrava et al. (2016) <DOI:10.1007/s11192-016-1842-4>.  The
Rao-Stirling diversity index is a well-established bibliometric indicator to
measure the interdisciplinarity of scientific publications.  Apart from the
obligatory dataset of publications with their respective references and a
taxonomy of disciplines that categorizes references as well as a measure of
similarity between the disciplines, the Rao-Stirling diversity index requires a
complete categorization of all references of a publication into disciplines.
Thus, it fails for a incomplete categorization; in this case, the robust
extension has to be used, which encodes the uncertainty caused by missing
bibliographic data as an uncertainty interval.  Classification / ACM - 2012:
Information systems ~ Similarity measures, Theory of computation ~
Quadratic       programming, Applied computing ~ Digital libraries and archives.")
   (license gpl3))
  )


(define-public r-xlsxjars
  (package
   (name "r-xlsxjars")
   (version "0.6.1")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "xlsxjars" version))
            (sha256
             (base32
              "1rka5smm7yqnhhlblpihhciydfap4i6kjaa4a7isdg7qjmzm3h9p"))))
   (properties `((upstream-name . "xlsxjars")))
   (build-system r-build-system)
   (propagated-inputs (list r-rjava))
   (home-page "https://cran.r-project.org/package=xlsxjars")
   (synopsis "Package required POI jars for the xlsx package")
   (description
    "The xlsxjars package collects all the external jars required for the xlxs
package.  This release corresponds to POI 3.10.1.")
   (license gpl3))
  )

;; Include in guix-science channel, but don't work
(define-public r-xlsx
  (package
   (name "r-xlsx")
   (version "0.6.5")
   (source (origin
            (method url-fetch)
            (uri (cran-uri "xlsx" version))
            (sha256
             (base32
              "01r1ngdm51w18bdan8h94r91m731knkf04zal4g67mx3fpa5x31p"))))
   (properties `((upstream-name . "xlsx")))
   (build-system r-build-system)
   (inputs (list openjdk))
   (propagated-inputs (list r-rjava r-xlsxjars))
   (native-inputs (list r-knitr))
   (home-page "https://github.com/colearendt/xlsx")
   (synopsis "Read, Write, Format Excel 2007 and Excel 97/2000/XP/2003 Files")
   (description
    "Provide R functions to read/write/format Excel 2007 and Excel 97/2000/XP/2003
file formats.")
   (license gpl3))
  )


(define-public python-backports.shutil-which 
  (package
   (name "python-backports.shutil-which")
   (version "3.5.2")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "backports.shutil_which" version))
            (sha256
             (base32
              "0cy16w2dpv110afncag8x1zhzy3yz0iypny4iagdiyp4rdkzafgy"))))
   (build-system pyproject-build-system)
   (home-page "https://github.com/minrk/backports.shutil_which")
   (synopsis "Backport of shutil.which from Python 3.3")
   (description "Backport of shutil.which from Python 3.3")
   (license #f))
  )


(define-public python-rchitect
  (package
   (name "python-rchitect")
   (version "0.3.40")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "rchitect" version))
            (sha256
             (base32
              "1dxq8xcc849ny2nx865wnjq5gf7pbpydnqkvbqi39jsdj72fap8w"))))
   (build-system pyproject-build-system)
   ;; FIXME: repair
   (arguments
    '(#:tests? #f))
   (propagated-inputs (list python-backports.shutil-which python-cffi
                            python-six))
   (native-inputs (list python-pytest python-pytest-cov python-pytest-mock))
   (home-page "https://github.com/randy3k/rchitect")
   (synopsis "Mapping R API to Python")
   (description "Mapping R API to Python")
   (license #f))
  )


(define-public python-radian
  (package
   (name "python-radian")
   (version "0.6.5")
   (source (origin
            (method url-fetch)
            (uri (pypi-uri "radian" version))
            (sha256
             (base32
              "0b4wzx4lkjzmxc3x1dyr3ig305bam9xmvdwq1fw36z9whbaandi1"))))
   (build-system pyproject-build-system)
   ;; FIXME: repair
   (arguments
    '(#:tests? #f))
   (propagated-inputs (list python-prompt-toolkit python-pygments
                            python-rchitect))
   (native-inputs (list python-coverage python-pexpect python-ptyprocess
			python-pyte python-pytest))
   (home-page "https://github.com/randy3k/radian")
   (synopsis "A 21 century R console")
   (description "This package provides a 21 century R console")
   (license #f))
  )



;;
;; Manifest
;;

(concatenate-manifests
 (list
  (specifications->manifest
   (list
    "r-minimal" ;; Environment for statistical computing and graphics  
    "r-dplyr" ;; Tools for working with data frames in R  
    "r-tidyr" ;; Tidy data with `spread()` and `gather()` functions 
    "r-ggplot2" ;; Implementation of the grammar of graphics  
    "r-here" ;; Simpler way to find files  
    "r-languageserver" ;; Language Server for R  
    "r-readtext" ;; Import and Handling for Plain and Formatted Text Files  
    "r-rselenium" ;; R bindings for Selenium WebDriver  
    "r-rvest" ;; Simple web scraping for R  
    "r-profvis" ;; Interactive visualizations for profiling R code
    ;;"r-tidyverse" ;; Install and load packages from the "Tidyverse"
    "r-foreach" ;; Foreach looping construct for R
    ;;"r-xlsx" ;; Read, Write, Format Excel 2007 and Excel 97/2000/XP/2003 Files
    ))

  (packages->manifest (list
                       r-refinr
                       r-robustrao
                       ;;r-xlsx ;; Read, Write, Format Excel 2007 and Excel 97/2000/XP/2003 Files
		       python-radian ;; "A 21 century R console"
                       ))
  ))
