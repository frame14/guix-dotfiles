;; From:
;; - https://unix.stackexchange.com/questions/733859/installing-neovim-v0-8-1-with-guix-makes-some-treesitter-parser-unusuable-becaus

(use-modules (guix packages)
	     (gnu packages vim)
	     (guix transformations))

(define transform
  (options->transformation
   '((with-c-toolchain . "neovim=gcc-toolchain@12"))))

(packages->manifest (list
                       (transform neovim)
                       ))
