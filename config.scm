;; Inspirations
;; https://gist.github.com/jespada/87284be94404e2001be4799f0c4e2a82


;; Indicate which modules to import to access the variables
;; used in this configuration.
(use-modules (gnu)
	     (nongnu packages linux) ;; linux kernel
	     (nongnu system linux-initrd) ;; firmware blob

	     (gnu packages wm)
	     (gnu packages lisp)
	     (gnu packages certs)
	     (gnu packages terminals)
	     (gnu packages kde-plasma)
	     (gnu packages suckless)
	     (gnu packages version-control)
	     (gnu packages fonts)
	     (gnu packages file-systems)
	     (gnu packages xorg)
	     (gnu packages admin)

	     (gnu services sound)
	     (gnu services virtualization)

	     (gnu system setuid)

	     ;;(dwl-guile packages)
	     ;;(dtao-guile packages)

	     )


(use-service-modules 
 cups
 file-sharing ;; transmission
 pm
 desktop
 networking
 ssh
 xorg
 nix ;; nix
 sddm
 databases    ;; postgresql
 docker
 )

(use-package-modules
 package-management ;; nix
 linux ;; light
 dunst ;; dunst
 xdisorg ;; redshift
 image-viewers ;; imv
 databases ;; postgresql
 display-managers ;; sddm themes
 )



(define my-keyboard-layout
  (keyboard-layout "ca" "fr"
                   #:options '("ctrl:nocaps" "altwin:menu_win")))

(define my-xorg-configuration
  (xorg-configuration
   (keyboard-layout my-keyboard-layout)
   (modules %default-xorg-modules)
   (resolutions '((2256 1504)))
   (extra-config '(;;"Section \"Device\"
                   ;;		Identifier \"Intel Graphics\"
                   ;;		Driver \"intel\"
                   ;;		Option \"TearFree\" \"true\"
                   ;;		EndSection"
                   ;;           Option \"DRI\" \"iris\"
                   ;;		Option \"AccelMethod\" \"sna\"

		   ;; Reduce tearing
		   ;; See https://community.frame.work/t/display-tearing-and-strange-artifacting/10217/10
		   ;; Intel driver
		   ;; Tearing is gone, but weird line in alacritty terminal
		   ;; "Section \"Device\"
                   ;;    Identifier \"Intel Graphics\"
                   ;;    Driver \"intel\"
                   ;;    Option \"TearFree\" \"true\"
                   ;;    Option \"AccelMethod\" \"sna\"
                   ;;    Option \"DRI\" \"iris\"
                   ;;  EndSection"

		   ;; Modestting driver
		   ;; Still screen tearing...
;; "Section \"Device\"
;;   Identifier \"Intel Graphics\"
;;   Driver \"modesetting\"
;;   Option \"TearFree\" \"true\"
;; EndSection"



		   ;; "Section \"InputClass\"
		   ;;    Identifier \"touchpad\"
		   ;;    Driver \"libinput\"
		   ;;    MatchIsTouchpad \"on\"
		   ;;    Option \"DisableWhileTyping\" \"on\"
		   ;;    Option \"Tapping\" \"on\"
		   ;; EndSection"
                   ;; ;; TODO: add delay rate to keyboard xorg configuration
                   ;;		   "Section \"InputClass\"
                   ;;		  		Identifier \"touchpad\"
                   ;;		  		Driver \"libinput\"
                   ;;	                        Option \"AutoRepeat\" \"300 50\"
                   ;;		  		EndSection"

		   ;; From https://community.frame.work/t/tracking-gnu-guix-system-on-the-framework/22459/10

		                "Section \"Device\"
				Identifier \"Intel Graphics\"
				Driver \"intel\"
				Option \"AccelMethod\" \"sna\"
				EndSection
"

				"Section \"Device\"
				Identifier \"0x43\"
				Driver \"intel\"
				Option \"Backlight\" \"intel_backlight\"
				EndSection
"
				
				"Section \"InputClass\"
				Identifier \"Touchpads\"
				Driver \"libinput\"
				MatchIsTouchpad \"on\"
				Option \"DisableWhileTyping\" \"on\"
				Option \"Tapping\" \"on\"
				Option \"MiddleEmulation\" \"on\"
				EndSection"
                   ))
   ))


(define %backlight-udev-rule
  (udev-rule
   "90-backlight.rules"
   (string-append "ACTION==\"add\", SUBSYSTEM==\"backlight\", "
                  "RUN+=\"/run/current-system/profile/bin/chgrp video /sys/class/backlight/%k/brightness\""
                  "\n"
                  "ACTION==\"add\", SUBSYSTEM==\"backlight\", "
                  "RUN+=\"/run/current-system/profile/bin/chmod g+w /sys/class/backlight/%k/brightness\"")))

(define %initial-backlight-udev-rule
  (udev-rule
   "81-backlight.rules"
   (string-append "SUBSYSTEM==\"backlight\", ACTION==\"add\", KERNEL==\"intel_backlight\", ATTR{brightness}=\"960\"")))

(define %my-desktop-services
  (modify-services %desktop-services
                   ;; Set initial backlight
                   (udev-service-type config =>
                                      (udev-configuration (inherit config)
                                                          (rules (cons* %backlight-udev-rule
                                                                        %initial-backlight-udev-rule
                                                                        (udev-configuration-rules config)))))
                   ;; Enable substitute for nonguix - should help with large packages, i.e. linux, firefox
                   ;; - nonguix provided by https://substitutes.nonguix.org/
                   ;; - guix-science provided by https://substitutes.guix.psychnotebook.org/
                   (guix-service-type config =>
                                      (guix-configuration
                                       (inherit config)
                                       (substitute-urls
                                        (append
                                         %default-substitute-urls
                                         (list "https://substitutes.nonguix.org"
                                               "https://substitutes.guix.psychnotebook.org"
                                               ;;      "https://guix.bordeaux.inria.fr"
                                               )))
                                       (authorized-keys
                                        (append
                                         %default-authorized-guix-keys
                                         (list
                                          (plain-file "substitutes.nonguix.org.pub" "
                                    (public-key
                                      (ecc
                                         (curve Ed25519)
                                         (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)
                                         )
                                      )")
                                          (plain-file "substitutes.guix.psychnotebook.org.pub" "
                                (public-key
                                  (ecc
                                (curve Ed25519)
                                (q #D4E1CAFAB105581122B326E89804E3546EF905C0D9B39F161BBD8ABB4B11D14A#)
                                )
                                  )")
                                          ;; (plain-file "substitutes.guix.bordeaux.inria.fr.pub" "
                                          ;;  (public-key
                                          ;;   (ecc
                                          ;;    (curve Ed25519)
                                          ;;    (q #89FBA276A976A8DE2A69774771A92C8C879E0F24614AAAAE23119608707B3F06#)))
                                          ;;     ")
                                          )))))

           ;;;; enable wayland for gdm and set xorg-configuration
                   ;;(gdm-service-type config =>
                   ;;		     (gdm-configuration
                   ;;		      (inherit config)
                   ;;		      (xorg-configuration my-xorg-configuration)
                   ;;		      (wayland? #t)))

           ;;;; prefer sddm
                   (delete gdm-service-type)

                   ;; prefer pipewire (local configuration)
                   (delete pulseaudio-service-type )
                   ))


(operating-system

 ;; nonguix support
 (kernel linux)
 (kernel-arguments
  (cons* "mem_sleep_default=deep" "nvme.noacpi=1" ;; more efficient sleep
         "i915.enable_psr=1" ;; reduce flickering
         "modprobe.blacklist=hid_sensor_hub" ;; enable changing brightness
         "quiet" ;; Disable most log messages
         "splash" ;; show splash screen
         %default-kernel-arguments))


 (initrd microcode-initrd)
 (firmware (list linux-firmware))

 (locale "en_CA.utf8")
 (timezone "America/New_York")
 (keyboard-layout my-keyboard-layout)
 (host-name "frame14")

 ;; The list of user accounts ('root' is implicit).
 (users (cons* (user-account
                (name "k8x1d")
                (comment "Kevin Kaiser")
                (group "users")
                (home-directory "/home/k8x1d")
                (supplementary-groups '("wheel"
                                        "netdev"
                                        "audio"
                                        "video"
                                        "input"
                                        ;;"davfs2"
					"docker"
					"kvm"
                                        )))
               %base-user-accounts))

 ;; Packages installed system-wide.  Users can also install packages
 ;; under their own account: use 'guix search KEYWORD' to search
 ;; for packages and 'guix install PACKAGE' to install a package.
 (packages (append (list awesome

                         ;; Plasma (don't work...
                         ;;plasma-desktop ;; smaller that plasma

                         ;; ;; dwl-guile
                         ;; dwl-guile
                         ;; dtao-guile

                         ;; Xorg
                         xinit xorg-server xf86-input-libinput xf86-video-fbdev

                         ;; Wayland
                         ;;seatd


                         ;; i3
                         i3-wm i3status i3lock-color

                         ;; sway
                         sway swaylock-effects
                         ;;swaylock

             ;;;; Velox
                         ;;velox
                         ;;swc


                         ;; Strumpwm
                         ;; sbcl stumpwm `(,stumpwm "lib") sbcl-stumpwm-ttf-fonts
                         sbcl stumpwm+slynk `(,stumpwm "lib") sbcl-stumpwm-ttf-fonts slock

                         ;; display server independent programs
                         light
                         dmenu
                         dunst
                         redshift-wayland
                         rofi-wayland
                         alacritty
                         imv

                         ;; Desktop
                         chili-sddm-theme

                         ;; Packages manager
                         ;;flatpak
                         nix

                         ;;
                         git
                         ;; WebDAV
                         davfs2

                         ;; Fonts
                         font-dejavu

                         ;; for HTTPS access
                         nss-certs
                         )
                   %base-packages))

 (setuid-programs (append (list
                           (setuid-program (program (file-append light "/bin/light"))))
                          %setuid-programs))

 
 ;; Below is the list of system services.  To search for available
 ;; services, run 'guix system search KEYWORD' in a terminal.
 (services
  (cons*

   ;;;; Gnome
   ;;(service gnome-desktop-service-type)

   ;; To configure OpenSSH, pass an 'openssh-configuration'
   ;; record as a second argument to 'service' below.
   (service openssh-service-type)

   ;; Printers
   (service cups-service-type
	    (cups-configuration
	     (web-interface? #t)))

   ;; Bluetooth
   (service bluetooth-service-type
            (bluetooth-configuration
             (experimental #t)))

   ;; Power management
   (service tlp-service-type)
   (service thermald-service-type) 

   ;; Nix support 
   (service nix-service-type)

   ;; PAM
   (service screen-locker-service-type
	    (screen-locker-configuration
	     (name "i3lock")
	     (program (file-append i3lock-color "/bin/i3lock"))))
   (service screen-locker-service-type
	    (screen-locker-configuration
	     (name "swaylock")
	     (program (file-append swaylock-effects "/bin/swaylock"))
	     (using-pam? #t)
	     (using-setuid? #f)))
   (service gnome-keyring-service-type)

   ;; Torrents
   (service transmission-daemon-service-type
            (transmission-daemon-configuration
             (download-dir "/extension/Multimedia/Torrents")
             (alt-speed-down (* 1024 2)) ;   2 MB/s
             (alt-speed-up 512)))          ; 512 kB/s

   ;; ;; Music
   ;; per user config was prefered
   ;; (service mpd-service-type
   ;;          (mpd-configuration
   ;;           (outputs
   ;;            (list (mpd-output
   ;;                   (name "pulse audio")
   ;;                   (type "pulse"))))
   ;;           ;;(user "k8x1d")
   ;;           (music-directory "~/Music")
   ;;           (playlist-directory "~/.config/mpd/playlists")
   ;;           (db-file "~/.config/mpd/database")
   ;;           (state-file "~/.config/mpd/state")
   ;;           (sticker-file "~/.config/mpd/sticker.sql")
   ;;           (default-port 6600)
   ;;           ))

   ;; Databases
   (service postgresql-service-type
            (postgresql-configuration
             (data-directory "/extension/Data/postgres/data")
             (postgresql postgresql-14)))
   (service postgresql-role-service-type
            (postgresql-role-configuration
             (roles
              (list (postgresql-role
                     (name "k8x1d")
                     (permissions '(createdb login superuser))
                     (create-database? #f))))))

   ;; ;; Login manager
   (service sddm-service-type
	    (sddm-configuration
	     (display-server "wayland")
	     ;;(theme "chili")
	     (theme "sddm-chili")
	     (themes-directory "/extension/Work/Documents/Developpement/Logiciels/frame14/sddm/themes")
	     (xorg-configuration my-xorg-configuration)
	     (faces-directory "/extension/Work/Documents/Developpement/Logiciels/frame14/sddm/faces")
	     ))

   (extra-special-file
    "/usr/bin/startx"
    (xorg-start-command
     ;;(xorg-configuration
     ;; (keyboard-layout my-keyboard-layout)
     ;; (modules %default-xorg-modules)
     ;; (resolutions '((2256 1504)))
     ;; (fonts (cons* font-gnu-unifont
     ;;		    font-terminus
     ;;		    %default-xorg-fonts))
     ;; )
     ;;   (xorg-configuration my-xorg-configuration)

     (xorg-configuration
      (keyboard-layout my-keyboard-layout)
      (modules %default-xorg-modules)
      (server xorg-server)
      (resolutions '((2256 1504)))
      (extra-config '(
                      "Section \"InputClass\"
            Identifier \"touchpad\"
            Driver \"libinput\"
            MatchIsTouchpad \"on\"
            Option \"DisableWhileTyping\" \"on\"
            Option \"Tapping\" \"on\"
            EndSection"
                      ))
      )
     ))

   (service xorg-server-service-type
            (xorg-configuration
             (keyboard-layout my-keyboard-layout)
             (modules %default-xorg-modules)
             (server xorg-server)
             (resolutions '((2256 1504)))
             (extra-config '(
                             "Section \"InputClass\"
        Identifier \"touchpad\"
        Driver \"libinput\"
        MatchIsTouchpad \"on\"
        Option \"DisableWhileTyping\" \"on\"
        Option \"Tapping\" \"on\"
        EndSection"
                             ))
             ))
   ;;;; Seat
   ;;(service seatd-service-type)

   ;; Docker
   (service docker-service-type )

   ;; Vitualization
   (service libvirt-service-type)
   (service virtlog-service-type
	    (virtlog-configuration
	     (max-clients 1000)))
   (service qemu-binfmt-service-type
	    (qemu-binfmt-configuration
	     (platforms (lookup-qemu-platforms "arm" "aarch64"))))
   (service qemu-guest-agent-service-type)


   ;;;; Xorg configuration, work only for gdm
   ;;(set-xorg-configuration my-xorg-configuration)

   ;; This is the default list of services we
   ;; are appending to.
   %my-desktop-services))

 (bootloader (bootloader-configuration
              (bootloader grub-efi-bootloader)
              (timeout 2)
              (targets (list "/boot/efi"))
              (theme (grub-theme
                      (inherit (grub-theme))
                      (resolution '(2256 . 1504))))
              (menu-entries (list
                             (menu-entry
                              (label "Arch Linux")
                              (linux "/vmlinuz-linux")
                              ;;(linux-arguments '("root=UUID=029852cf-8ce8-4297-8881-0f562522db87 ro quiet"))
                              (linux-arguments '("root=/dev/nvme0n1p5 rw quiet"))
                              ;;(initrd '("/boot/intel-ucode.img" "/boot/initramfs-linux.img")))
                              (initrd  "/initramfs-linux.img"))
                             ))
              (keyboard-layout my-keyboard-layout)))
 (swap-devices (list (swap-space
                      (target (uuid
                               "7f10b527-b7d0-4569-88dd-4a223d84f34e")))))

 ;; The list of file systems that get "mounted".  The unique
 ;; file system identifiers there ("UUIDs") can be obtained
 ;; by running 'blkid' in a terminal.
 (file-systems (cons* 
                (file-system
                 (mount-point "/extension")
                 (create-mount-point? #t)
                 (device (uuid
                          "db47345d-e655-4ecf-acc1-e003bffc35c1"
                          'ext4))
                 (type "ext4"))
                (file-system
                 (mount-point "/home")
                 (device (uuid
                          "fc84804a-1b19-4e24-8182-12ac99471975"
                          'ext4))
                 (type "ext4"))
                (file-system
                 (mount-point "/")
                 (device (uuid
                          "ebaf01fd-8f5c-49c2-b4e3-317b15258964"
                          'ext4))
                 (type "ext4"))
                (file-system
                 (mount-point "/boot/efi")
                 (device (uuid "5AF0-8AE7"
                               'fat32))
                 (type "vfat"))
                ;;	(file-system
                ;;	 (mount-point "https://use04.thegood.cloud/remote.php/dav/files/k8x1d@proton.me/ /home/k8x1d/nextcloud")
                ;;	 )
                ;;davfs user,rw,auto 0 0

                %base-file-systems))
 )

